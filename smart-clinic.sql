-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6333
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for smart-clinic
DROP DATABASE IF EXISTS `smart-clinic`;
CREATE DATABASE IF NOT EXISTS `smart-clinic` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `smart-clinic`;

-- Dumping structure for table smart-clinic.clinics
DROP TABLE IF EXISTS `clinics`;
CREATE TABLE IF NOT EXISTS `clinics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `doctor_in_charge_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_clinics_doctors` (`doctor_in_charge_id`) USING BTREE,
  CONSTRAINT `FK_clinics_doctors` FOREIGN KEY (`doctor_in_charge_id`) REFERENCES `doctors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.clinics: ~2 rows (approximately)
DELETE FROM `clinics`;
/*!40000 ALTER TABLE `clinics` DISABLE KEYS */;
INSERT INTO `clinics` (`id`, `title`, `doctor_in_charge_id`) VALUES
	(1, 'Cardiac Clinic', 6),
	(2, 'ENT Clinic 1', 1);
/*!40000 ALTER TABLE `clinics` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.clinic_patients
DROP TABLE IF EXISTS `clinic_patients`;
CREATE TABLE IF NOT EXISTS `clinic_patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clinic_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `since` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_clinic_patients_clinics` (`clinic_id`),
  KEY `FK_clinic_patients_patients` (`patient_id`),
  CONSTRAINT `FK_clinic_patients_clinics` FOREIGN KEY (`clinic_id`) REFERENCES `clinics` (`id`),
  CONSTRAINT `FK_clinic_patients_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.clinic_patients: ~5 rows (approximately)
DELETE FROM `clinic_patients`;
/*!40000 ALTER TABLE `clinic_patients` DISABLE KEYS */;
INSERT INTO `clinic_patients` (`id`, `clinic_id`, `patient_id`, `since`) VALUES
	(1, 1, 5, '2021-06-01'),
	(2, 1, 1, '2021-07-03'),
	(4, 1, 2, '2021-07-03'),
	(5, 2, 1, '2021-07-03'),
	(6, 2, 3, '2021-07-03');
/*!40000 ALTER TABLE `clinic_patients` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.clinic_visits
DROP TABLE IF EXISTS `clinic_visits`;
CREATE TABLE IF NOT EXISTS `clinic_visits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clinic_patient_id` int(10) unsigned NOT NULL,
  `visit_date` date NOT NULL,
  `token_number` int(11) DEFAULT NULL,
  `doctor_remarks` text DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` enum('ACTIVE','COMPLETED','CANCELLED','MISSED') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `FK_clinic_visits_clinic_patients` (`clinic_patient_id`),
  CONSTRAINT `FK_clinic_visits_clinic_patients` FOREIGN KEY (`clinic_patient_id`) REFERENCES `clinic_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.clinic_visits: ~0 rows (approximately)
DELETE FROM `clinic_visits`;
/*!40000 ALTER TABLE `clinic_visits` DISABLE KEYS */;
/*!40000 ALTER TABLE `clinic_visits` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.doctors
DROP TABLE IF EXISTS `doctors`;
CREATE TABLE IF NOT EXISTS `doctors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `speciality_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_doctors_doctor_speciality` (`speciality_id`),
  CONSTRAINT `FK_doctors_doctor_speciality` FOREIGN KEY (`speciality_id`) REFERENCES `doctor_speciality` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.doctors: ~5 rows (approximately)
DELETE FROM `doctors`;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` (`id`, `name`, `email`, `phone`, `dob`, `speciality_id`) VALUES
	(1, 'Kumaran', 'kumar@gmail.com', '0123456789', '1981-01-02', 1),
	(4, 'Rajan Maran', 'ranjan.maran@hotmail.com', '0774561238', '1974-02-16', 2),
	(5, 'Blue Berry', 'updated@hello.com', '0745124801', '1980-02-16', 4),
	(6, 'Bananan Kiwi', 'banana.kiwi@hotmail.com', '0774561236', '1987-02-16', 3),
	(7, 'Kiwi', 'kiwi@hello.com', '0774512745', '1969-04-24', 4),
	(8, 'ðŸ…°â™¿ðŸ‘¨â€ðŸ¦¼ðŸ§¾', 'hello@hell.com', '', '2021-06-15', 3);
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.doctor_speciality
DROP TABLE IF EXISTS `doctor_speciality`;
CREATE TABLE IF NOT EXISTS `doctor_speciality` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `speciality` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.doctor_speciality: ~4 rows (approximately)
DELETE FROM `doctor_speciality`;
/*!40000 ALTER TABLE `doctor_speciality` DISABLE KEYS */;
INSERT INTO `doctor_speciality` (`id`, `speciality`) VALUES
	(1, 'ENT'),
	(2, 'Skin Care'),
	(3, 'Cardiology'),
	(4, 'Epitimology');
/*!40000 ALTER TABLE `doctor_speciality` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.patients
DROP TABLE IF EXISTS `patients`;
CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `nic` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_phone` varchar(20) DEFAULT NULL,
  `gender` enum('MALE','FEMALE','OTHER') NOT NULL,
  `login_name` varchar(50) DEFAULT NULL,
  `login_pass` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.patients: ~4 rows (approximately)
DELETE FROM `patients`;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` (`id`, `full_name`, `dob`, `age`, `phone`, `nic`, `address`, `guardian_name`, `guardian_phone`, `gender`, `login_name`, `login_pass`) VALUES
	(1, 'Thanos', '1988-10-04', NULL, '', '845124571454', '', '', '', 'MALE', '1THANO', 'a9604f'),
	(2, 'Sriram Ram', '1974-06-15', 35, '077456123', '741254658V', 'Somewhere, Someplace', 'Avengers', '965124571', 'MALE', '2SRIRA', '3ea3ea'),
	(3, 'Mango Jango', '1963-09-18', NULL, '065-4512457', '0124515487', '#23, Main road,\nSomeplace,\nColombo', 'Thanos', 'Galaxy', 'FEMALE', '3MANGO', 'e4de64'),
	(5, 'Peter Parker', NULL, 35, '077456123', '741254658V', 'Somewhere, Someplace', 'Avengers', '965124571', 'MALE', '5PETER', '9d3b9d');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `role` enum('ADMIN','STAFF','USER') NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password_hash`, `email`, `full_name`, `role`) VALUES
	(1, 'admin', '$2y$10$JN/JQbRZ8zj6ReU5StNgc.AXIWuw7c8OexEk1Hlnh7/TBkuDzdyp2', 'admin@hello.com', 'Administrator', 'ADMIN'),
	(3, 'staff', '$2y$10$SgxRXcPymfc1Yqds5hV7FOeJ77Zo3ksREobZ1267WY9ZShS8G0YSG', 'staff@hello.com', 'Staff Safron', 'STAFF');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table smart-clinic.users_auth_keys
DROP TABLE IF EXISTS `users_auth_keys`;
CREATE TABLE IF NOT EXISTS `users_auth_keys` (
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `valid_till` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_users_auth_keys_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table smart-clinic.users_auth_keys: ~1 rows (approximately)
DELETE FROM `users_auth_keys`;
/*!40000 ALTER TABLE `users_auth_keys` DISABLE KEYS */;
INSERT INTO `users_auth_keys` (`user_id`, `auth_key`, `valid_till`) VALUES
	(1, '0b0f8a942489852d0fd4bec44b47441a', '2021-07-22 08:23:51'),
	(3, '8feb69b03b6d8bd18283283f56aacdfd', '2021-08-14 00:34:43');
/*!40000 ALTER TABLE `users_auth_keys` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
